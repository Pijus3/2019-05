<?php

class Data 
{
    private $username = "root";
    private $password = "";
    private $host = "localhost";
    private $database = "custom_form";

    private $conn = null;
    private $array = [];

    public function __construct()
    {
        if ( $this->conn == null )
        {
            try
            {
               return $this->conn = new PDO("mysql:dbname=$this->database;host=$this->host", $this->username, $this->password);
            }
            catch (PDOException $e)
            {
                echo 'Connection failed: '.$e->getMessage();
            }
            
        }
        else
        {
            return $this->conn;
        }
    }

    public function getRows()
    {
        $sql = $this->conn->prepare (" SELECT `comment_1`, `rate_1`, `comment_2`, `rate_2`, `comment_3`, `rate_3`, `timestamp` FROM `ivertinimai` ");
        $sql -> execute();
        $getData = $sql->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($getData);
    }


}
$petras = new Data();
print_r($petras->getRows());