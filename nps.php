<?php

$username = "root";
$password = "";
$database = "custom_form";
$host = "localhost";

try 
{
    $conn = new PDO("mysql:dbname=$database;host=$host", $username, $password);
}
catch (PDOException $e)
{
    echo 'Connection failed: '.$e->getMessage();
}

if( isset($_POST['comment-1']) || isset($_POST['comment-1']) || isset($_POST['comment-3']) || ( isset($_POST['rate-1']) && isset($_POST['rate-2']) && isset($_POST['rate-3']) ) )
{
    //$jsArray = json_decode($_POST['jsArray']);
    $comment_1 = filter_var($_POST['comment-1'], FILTER_SANITIZE_STRING);
    $comment_2 = filter_var($_POST['comment-2'], FILTER_SANITIZE_STRING);
    $comment_3 = filter_var($_POST['comment-3'], FILTER_SANITIZE_STRING);

    $rate_1 = filter_var($_POST['rate-1'], FILTER_SANITIZE_NUMBER_INT);
    $rate_2 = filter_var($_POST['rate-2'], FILTER_SANITIZE_NUMBER_INT);
    $rate_3 = filter_var($_POST['rate-3'], FILTER_SANITIZE_NUMBER_INT);

    //$rate_1 = $jsArray[0];
    //$rate_2 = $jsArray[1];
    //$rate_3 = $jsArray[2];
    
    $sql = "INSERT INTO `ivertinimai`(`comment_1`, `rate_1`, `comment_2`, `rate_2`, `comment_3`, `rate_3`) VALUES ( '$comment_1', '$rate_1', '$comment_2', '$rate_2', '$comment_3', '$rate_3' )";
    $conn->exec($sql) or die(print_r($conn->errorInfo(), true));

    $arr = [
        'comment-1' => $comment_1,
        'rate-1' => $rate_1,
        'comment-2' => $comment_2,
        'rate-2' => $rate_2,
        'comment-3' => $comment_3,
        'rate-3' => $rate_3
    ];
    $result = json_encode($arr);

    $data = array (
        'form_id' => '36',
        'data' => $result
        );
        
        $params = '';
        foreach($data as $key=>$value){
            $params .= $key.'='.$value.'&';
        }
            $params = trim($params, '&');

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://api.avitela.lt/valdymas/index.php/api/fill-custom-form');
    curl_setopt($ch, CURLOPT_POST, count($data));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($ch);
    //var_dump($response);

    curl_close ($ch);

}
else 
{
    echo "Tusti laukeliai";
}